import numpy as np

def svd(X, min_variance_explained=0.7):

    mean = np.mean(X, axis=0)  # calculate the mean of each column
    X -= mean  # Center data

    # Singular Value Decomposition
    left_singular_vectors, eigenvalues, eigenvectors = np.linalg.svd(X, full_matrices=False)

    # Get variance explained by singular values
    n_samples, n_features = X.shape
    S = eigenvalues
    explained_variance_ = (S ** 2) / (n_samples - 1)
    total_var = explained_variance_.sum()
    explained_variance_ratio_ = explained_variance_ / total_var
    cumsum_explained_variance_ratio = np.cumsum(explained_variance_ratio_)

    # Limit count of components, to the min explained variance ratio (%)
    ideal_num_of_components = np.argmax(cumsum_explained_variance_ratio >= min_variance_explained) + 1
    eigenvalues = eigenvalues[:ideal_num_of_components]
    eigenvectors = eigenvectors[:ideal_num_of_components]

    return [mean, eigenvalues, eigenvectors, round(cumsum_explained_variance_ratio[ideal_num_of_components - 1], 4)]
