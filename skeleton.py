import copy, os, math
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mahalanobis import get_mahalanobis_grayscale
from boundaryPerpenticularSampleVector import get_samples_along_boundary
from procrustes import procrustes_transform, inverse_procrustes_transform, get_generalized_procrustes_analysis_transform
from singular_value_decomposition import svd
from haarCascadeClassifier import get_new_incisor_starting_position

verbose = False
showPlots = False
plotEveryIterations = 10
plotAllTrainingPoints = False
plotPcaComponents = False
plotMinLambdaMaxLambda = False
storeBinaryDetection = True


def plot_min_lambda_max_lambda(b, eigenvalues, eigenvectors, mean2, limit1, limit2):
    mean_plus_lambda = []
    labels = []
    beta = copy.copy(b)
    beta[0] = limit1 * math.sqrt(eigenvalues[0])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_0 = {} * sqrt(lambda_0)".format(limit1))
    beta[0] = -limit1 * math.sqrt(eigenvalues[0])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_0 = - {} * sqrt(lambda_0)".format(limit1))
    plot_incisor_lambda(mean2, mean_plus_lambda, labels, "beta0_sqrt1")

    mean_plus_lambda = []
    labels = []
    beta[0] = limit2 * math.sqrt(eigenvalues[0])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_0 = {} * sqrt(lambda_0)".format(limit2))
    beta[0] = -limit2 * math.sqrt(eigenvalues[0])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_0 = -{} * sqrt(lambda_0)".format(limit2))
    plot_incisor_lambda(mean2, mean_plus_lambda, labels, "beta0_sqrt2")

    beta = copy.copy(b)
    mean_plus_lambda = []
    labels = []
    beta[1] = limit1 * math.sqrt(eigenvalues[1])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_1 = {} * sqrt(lambda_1)".format(limit1))
    beta[1] = - limit1 * math.sqrt(eigenvalues[1])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_1 = - {} * sqrt(lambda_1)".format(limit1))
    plot_incisor_lambda(mean2, mean_plus_lambda, labels, "beta1_sqrt1")

    mean_plus_lambda = []
    labels = []
    beta[1] = limit2 * math.sqrt(eigenvalues[1])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_1 = {} * sqrt(lambda_1)".format(limit2))
    beta[1] = - limit2 * math.sqrt(eigenvalues[1])
    mean_plus_lambda.append(np.add(mean2, np.matmul(beta, eigenvectors)))
    labels.append("beta_1 = -{} * sqrt(lambda_1)".format(limit2))
    plot_incisor_lambda(mean2, mean_plus_lambda, labels, "beta1_sqrt2")


def plot_radiograph(mean_1d, Ynew_1d, test_image, x_offset, y_offset, h, w, suggested_landmark_coordinates_1d, iteration):
    plt.figure(1, figsize=(8, 6))  # Resolution 800 x 600
    plt.title('Landmarks incisor {} iter: {} \n mahalanobis suggestions'.format(incisors[0], iteration))
    plt.imshow(test_image[y_offset:y_offset + h, x_offset:x_offset + w], cmap=cm.Greys_r)
    # Plot mean shape of incisor
    x_all = mean_1d[:len(mean_1d) / 2]  # Even: x1, ..., xn
    y_all = mean_1d[len(mean_1d) / 2:]  # Odd : y1, ..., xn
    x_all = np.subtract(x_all, x_offset)
    y_all = np.subtract(y_all, y_offset)
    plt.scatter(x_all, y_all, marker="o", facecolors='none', edgecolors='r', s=30,
                label='Mean Incisor {}\n boundary'.format(incisors[0]))
    # plt.plot(x_all, y_all, '--r', linewidth=0.4)
    # Plot suggested landmarks
    x_all = suggested_landmark_coordinates_1d[:len(suggested_landmark_coordinates_1d) / 2]  # Even: x1, ..., xn
    y_all = suggested_landmark_coordinates_1d[len(suggested_landmark_coordinates_1d) / 2:]  # Odd : y1, ..., xn
    x_all = np.subtract(x_all, x_offset)
    y_all = np.subtract(y_all, y_offset)
    plt.scatter(x_all, y_all, facecolors='b', marker="+", edgecolors='b', s=30,
                label='Radiograph {}\n suggestions'.format(test_radio_graph))
    # plt.plot(x_all, y_all, 'b', linewidth=0.4)
    # Plot suggested landmarks
    x_all = Ynew_1d[:len(Ynew_1d) / 2]  # Even: x1, ..., xn
    y_all = Ynew_1d[len(Ynew_1d) / 2:]  # Odd : y1, ..., xn
    x_all = np.subtract(x_all, x_offset)
    y_all = np.subtract(y_all, y_offset)
    plt.scatter(x_all, y_all, facecolors='g', marker="x", edgecolors='g', s=30,
                label='Ynew_1d {}\n suggestions'.format(test_radio_graph))
    plt.plot(x_all, y_all, 'g', linewidth=0.8)
    # Plot legend etc
    # plt.legend(bbox_to_anchor=(0.9, 0.9), bbox_transform=plt.gcf().transFigure)
    plt.legend()
    plt.grid(True)
    plt.axis('scaled')
    plt.xlabel("x")
    plt.ylabel("y")
    # plt.figure(2, figsize=(12, 10))  # Resolution 1600 x 1200
    plt.savefig('radiograph_landmarks_incisor_{}_iter_{}_mahalanobis_suggestions.png'.format(incisors[0], iteration), bbox_inches='tight')
    plt.show()


def plot_incisor(mean_1d, Ynew_1d, x_offset, y_offset, suggested_landmark_coordinates_1d, iteration):
    plt.title('Landmarks  incisor {} iter: {} \n mahalanobis suggestions'.format(incisors[0], iteration))
    # Plot mean shape of incisor
    x_all = mean_1d[:len(mean_1d) / 2]  # Even: x1, ..., xn
    y_all = mean_1d[len(mean_1d) / 2:]  # Odd : y1, ..., xn
    plt.scatter(x_all, y_all, marker="o", facecolors='none', edgecolors='r', s=10,
                label='Mean Incisor {}\n boundary'.format(incisors[0]))
    plt.plot(x_all, y_all, '--r', linewidth=0.4)
    # Plot suggested landmarks
    x_all = suggested_landmark_coordinates_1d[:len(suggested_landmark_coordinates_1d) / 2]  # Even: x1, ..., xn
    y_all = suggested_landmark_coordinates_1d[len(suggested_landmark_coordinates_1d) / 2:]  # Odd : y1, ..., xn
    plt.scatter(x_all, y_all, facecolors='b', marker="+", edgecolors='b', s=5,
                label='Radiograph {}\n suggestions'.format(test_radio_graph))
    # plt.plot(x_all, y_all, 'b', linewidth=0.4)
    # Plot suggested landmarks
    x_all = Ynew_1d[:len(Ynew_1d) / 2]  # Even: x1, ..., xn
    y_all = Ynew_1d[len(Ynew_1d) / 2:]  # Odd : y1, ..., xn
    # x_all = np.subtract(x_all, x_offset)
    # y_all = np.subtract(y_all, y_offset)
    plt.scatter(x_all, y_all, facecolors='g', marker="x", edgecolors='g', s=30,
                label='Ynew_1d {}\n suggestions'.format(test_radio_graph))
    # plt.plot(x_all, y_all, 'g', linewidth=0.4)
    # Plot legend etc
    # plt.legend(bbox_to_anchor=(0.9, 0.9), bbox_transform=plt.gcf().transFigure)
    plt.legend()
    plt.grid(True)
    plt.axis("on")
    plt.axis('scaled')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig('landmarks_incisor_{}_iter_{}_mahalanobis_suggestions.png'.format(incisors[0], iteration), bbox_inches='tight')
    plt.show()


def plot_incisor_training_points(mean_1d, all_landmark_coordinates_1d_all):
    plt.title('Landmarks incisor {} \n  all data (blue) and mean (red)'.format(1))
    # Plot all_landmark_coordinates_1d
    for all_landmark_coordinates_1d in all_landmark_coordinates_1d_all:
        x_all = all_landmark_coordinates_1d[:len(all_landmark_coordinates_1d) / 2]  # Even: x1, ..., xn
        y_all = all_landmark_coordinates_1d[len(all_landmark_coordinates_1d) / 2:]  # Odd : y1, ..., xn
        plt.scatter(x_all, y_all, facecolors='b', marker=".", edgecolors='b', s=2)
    # Plot mean shape of incisor
    x_all = mean_1d[:len(mean_1d) / 2]  # Even: x1, ..., xn
    y_all = mean_1d[len(mean_1d) / 2:]  # Odd : y1, ..., xn
    plt.scatter(x_all, y_all, marker="o", facecolors='r', edgecolors='r', s=20)
    # Plot legend etc
    plt.grid(True)
    plt.axis("on")
    plt.axis('scaled')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig('landmarks_incisor_{}_alldata_and_mean.png'.format(1), bbox_inches='tight')
    plt.show()


def plot_pca_components(pca_1, pca_2, comp_1, comp_2):
    plt.title('Incisor {} Principal Components {} & {}'.format(1, comp_1 + 1, comp_2 + 1))
    # Plot mean shape of incisor
    plt.scatter(pca_1, pca_2, marker="o", facecolors='r', edgecolors='r', s=20)
    # Plot legend etc
    plt.grid(True)
    plt.axis("on")
    plt.axis('scaled')
    plt.xlabel("PCA {}".format(comp_1 + 1))
    plt.ylabel("PCA {}".format(comp_2 + 1))
    plt.savefig('incisor{}_pca_components_{}_{}.png'.format(1, comp_1 + 1, comp_2 + 1), bbox_inches='tight')
    plt.show()


def plot_incisor_lambda(mean_1d, all_landmark_coordinates_1d_all, labels, fileNameDistinction):
    plt.title('Landmarks  incisor {} \n PCA limits'.format(1))
    # Plot mean shape of incisor
    x_all = mean_1d[:len(mean_1d) / 2]  # Even: x1, ..., xn
    y_all = mean_1d[len(mean_1d) / 2:]  # Odd : y1, ..., xn
    plt.scatter(x_all, y_all, marker="o", facecolors='none', edgecolors='r', s=10,
                label='Mean Incisor {}\n boundary'.format(incisors[0]))
    plt.plot(x_all, y_all, '--r', linewidth=0.4)
    # Plot suggested landmarks
    label_id = 0
    for suggested_landmark_coordinates_1d in all_landmark_coordinates_1d_all:
        x_all = suggested_landmark_coordinates_1d[:len(suggested_landmark_coordinates_1d) / 2]  # Even: x1, ..., xn
        y_all = suggested_landmark_coordinates_1d[len(suggested_landmark_coordinates_1d) / 2:]  # Odd : y1, ..., xn
        # plt.scatter(x_all, y_all, facecolors='b', marker="+", edgecolors='b', s=5,
        #             label='Radiograph {}\n suggestions'.format(test_radio_graph))
        # plt.plot(x_all, y_all, 'b', linewidth=0.4)
        plt.scatter(x_all, y_all)
        plt.plot(x_all, y_all, label='{}'.format(labels[label_id]))
        label_id += 1
    # Plot legend etc
    plt.legend()
    plt.grid(True)
    plt.axis("on")
    plt.axis('scaled')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.savefig('landmarks_incisor{}_pca_limits_{}.png'.format(1, fileNameDistinction), bbox_inches='tight')
    plt.show()


def do_mahalanobis_landmark_samples_coordinates(mean_landmark_coordinates, landmarks_samples_coordinates, train_images, test_image, m, k):

    if verbose:
        print "mean_landmark_coordinates: {}".format(mean_landmark_coordinates)

    mean_landmark_grayscale_values = []
    for landmark_id in range(len(landmarks_samples_coordinates)):
        landmark_sample_coordinates = landmarks_samples_coordinates[landmark_id]
        landmark_sample_grayscale_values = []
        for sample_id in range(len(landmark_sample_coordinates)):
            landmark_coordinate_sample = landmark_sample_coordinates[sample_id]
            if landmark_coordinate_sample[0] < len(train_images[sample_id]) and landmark_coordinate_sample[1] < len(train_images[sample_id][0]):
                landmark_sample_grayscale_values.append(
                    train_images[sample_id][landmark_coordinate_sample[0]][landmark_coordinate_sample[1]])
            else:  # Out of range: append zero
                landmark_sample_grayscale_values.append(0)

        mean_landmark = np.mean(landmark_sample_grayscale_values)
        mean_landmark_grayscale_values.append(mean_landmark)
    if verbose:
        print "mean_landmark_grayscale_values: {}".format(mean_landmark_grayscale_values)

    images_sample_coordinates_along_landmark = []  # 2k + 1 sample coordinates per landmark
    images_samples_along_landmark = []  # 2k + 1 samples per landmark
    for image_id in range(len(train_images)):
        image_samples_along_landmark = []
        image_sample_coordinates_along_landmark = []
        for landmark_id in range(len(landmarks_samples_coordinates)):
            landmark_coordinate = landmarks_samples_coordinates[landmark_id][image_id]
            sample_grid_coordinates, sample_values = get_samples_along_boundary(landmark_coordinate,
                                                                                mean_landmark_coordinates, 2 * k + 1,
                                                                                train_images[image_id])
            image_samples_along_landmark.append(sample_values)
            image_sample_coordinates_along_landmark.append(sample_grid_coordinates)
        images_samples_along_landmark.append(image_samples_along_landmark)
        images_sample_coordinates_along_landmark.append(image_sample_coordinates_along_landmark)

    all_landmark_samples_along = []
    for landmark_id in range(len(landmarks_samples_coordinates)):
        landmark_samples_along = []
        for image_id in range(len(train_images)):
            landmark_samples_along.append(images_samples_along_landmark[image_id][landmark_id])
        all_landmark_samples_along.append(landmark_samples_along)

    all_landmark_point_samples = []

    suggested_landmark_coordinates = []
    for landmark_id in range(len(mean_landmark_coordinates)):
        landmark_point = mean_landmark_coordinates[landmark_id]
        image = test_image
        sample_grid_coordinates, sample_values = get_samples_along_boundary(landmark_point, mean_landmark_coordinates,
                                                                            m, image)
        all_landmark_point_samples.append([sample_grid_coordinates, sample_values])
        if verbose:
            print "m: {}".format(m)
            print "landmark_point: {}".format(landmark_point)
            print "sample_grid_coordinates: {}".format(sample_grid_coordinates)
            print "sample_values: {}".format(sample_values)

        # Mahalanobis
        all_landmark_samples = all_landmark_samples_along[landmark_id]
        current_sample_m = sample_values
        min_sample, min_distance, offset = get_mahalanobis_grayscale(all_landmark_samples, current_sample_m)
        if verbose:
            print "Landmark {}, Min Mahalanobis distance {} (offset: {}) with (normalized) {}  vs all {}".format(
                landmark_id, min_distance, offset, min_sample, all_landmark_samples)
            print "Suggested new coordinate from: {} to: {} (offset {})"\
                .format(landmark_point, sample_grid_coordinates[offset + k], offset)
        if offset + k < len(sample_grid_coordinates):
            suggested_landmark_coordinates.append(sample_grid_coordinates[offset + k])
        else:
            suggested_landmark_coordinates.append(sample_grid_coordinates[offset])

    return suggested_landmark_coordinates


def get_de_noised_image(image_path):
    gray = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2GRAY)
    gray = cv2.medianBlur(gray, 9)
    gray = cv2.GaussianBlur(gray, (9, 9), 0)
    return gray


def get_limited_beta(b2, eigenvalues, percentage):
    if len(b2) == len(eigenvalues) and len(b2) > 0:
        for z in range(len(b2)):
            if b2[z] > percentage * math.sqrt(eigenvalues[z]):
                if verbose:
                    print"beta[{}] = {} eigenvalues limits of allowable shapes {} < beta_{} < {}" \
                        .format(z, b2[z], - percentage * math.sqrt(eigenvalues[z]), z,
                                percentage * math.sqrt(eigenvalues[z]))
                b2[z] = percentage * math.sqrt(eigenvalues[z])
            if b2[z] < -  percentage * math.sqrt(eigenvalues[z]):
                if verbose:
                    print"beta[{}] = {} eigenvalues limits of allowable shapes {} < beta_{} < {}" \
                        .format(z, b2[z], - percentage * math.sqrt(eigenvalues[z]), z,
                                percentage * math.sqrt(eigenvalues[z]))
                b2[z] = - percentage * math.sqrt(eigenvalues[z])
    return b2


def do_boundary_mahalanobis(incisors, radio_graphs, test_radio_graph, k, m, max_iterations):

    dir_incisor_landmarks = "data/text/landmarks"
    names_landmarks = ["landmarks{}-{}.txt".format(j, n) for j in radio_graphs for n in incisors]
    x_offset = []
    for incisor_landmarks in names_landmarks:  # Load all n landmarks for this radiograph as 1D vector of 2*n elements
        # An introduction to Active Shape models, Page 6: For a 2D image with n landmark points {(xi, yi)},
        #  for a single example as the 2n element vector x where x = transpose(x1, ..., xn, y1, ..., yn)
        f = open(dir_incisor_landmarks + '/' + incisor_landmarks, 'r')
        values = f.readlines()
        f.close()
        values = map(int, map(float, values))  # String to float values
        x_offset.append(values[0:][::2] + values[1:][::2])  # Merge x and y values: x1, ..., xn, y1, ..., yn


    grid_x = copy.copy(x_offset)
    grid_2d = [np.reshape(row, (int(len(row) / 2), 2), order='F') for row in grid_x]
    landmarks_samples_coordinates = np.swapaxes(grid_2d, 0, 1)

    dir_radiographs = "data/images/radiographs"
    names_radiographs = ["{}{}.tif".format(j / 10, j % 10) for j in radio_graphs]
    train_images = [get_de_noised_image(dir_radiographs + '/' + radiograph_image_name) for radiograph_image_name in names_radiographs]

    dir_test_radiographs = dir_radiographs
    if test_radio_graph > 14:
        dir_test_radiographs = dir_test_radiographs + '/extra'
    test_image = get_de_noised_image(dir_test_radiographs + '/' + "{}{}.tif".format(test_radio_graph / 10, test_radio_graph % 10))

    suggested_landmark_coordinates_2d = []
    for iteration in range(max_iterations):
        if iteration == 0:
            new_landmark_coordinates = np.mean(landmarks_samples_coordinates, axis=1).astype(int)
            mean_1d = np.ndarray.flatten(new_landmark_coordinates, order='F')

            # Get haarCascade location suggestion
            min_x = np.min(mean_1d[:len(mean_1d) / 2])  # Even: x1, ..., xn
            min_y = np.min(mean_1d[len(mean_1d) / 2:])  # Odd : y1, ..., xn
            x_transform, y_transform = get_new_incisor_starting_position(min_x, min_y,
                np.max(mean_1d[:len(mean_1d) / 2]) - min_x, np.max(mean_1d[len(mean_1d) / 2:]) - min_y,
                    test_image, 0.9, 0.15)

            if x_transform != 0 or y_transform != 0: # If found new starting incisor location
                grid_x = np.array(grid_x)
                for row in grid_x:
                    row[:len(row) / 2] += x_transform
                    row[len(row) / 2:] += y_transform

                # Update coordinates on all grids: grid_x ->...
                grid_2d = [np.reshape(row, (int(len(row) / 2), 2), order='F') for row in grid_x]
                landmarks_samples_coordinates = np.swapaxes(grid_2d, 0, 1)
                new_landmark_coordinates = np.mean(landmarks_samples_coordinates, axis=1).astype(int)
                mean_1d = np.ndarray.flatten(new_landmark_coordinates, order='F')
                print("Haar cascade transform")

        else:
            new_landmark_coordinates = suggested_landmark_coordinates_2d

        suggested_landmark_coordinates_2d = do_mahalanobis_landmark_samples_coordinates(new_landmark_coordinates,
            landmarks_samples_coordinates, train_images, test_image, m, k)

        suggested_landmark_coordinates_1d = np.ndarray.flatten(np.array(suggested_landmark_coordinates_2d), order='F')

        #  SVD & mean model - Calculate the eigenvectors of X
        x = get_generalized_procrustes_analysis_transform(grid_x)  # Generalized Procrustes Analysis (GPA) transform on samples
        x2 = copy.copy(x)
        reconstruction_min_variance_explained = 0.98
        mean, eigenvalues, eigenvectors, var_explained = svd(x, reconstruction_min_variance_explained)
        mean2 = copy.copy(mean)
        mean = np.reshape(mean, (int(len(mean) / 2), 2), order='F')

        if plotPcaComponents:
            plot_pca_components(eigenvectors[0], eigenvectors[1], 0, 1)
            plot_pca_components(eigenvectors[0], eigenvectors[2], 0, 2)
            plot_pca_components(eigenvectors[0], eigenvectors[3], 0, 3)
            plot_pca_components(eigenvectors[1], eigenvectors[2], 1, 2)

        # Procrustes
        Y = np.array(new_landmark_coordinates)
        residual_sse, y, procrustes_transformed = procrustes_transform(mean, Y)

        y = np.ndarray.flatten(y, order='F')
        eigenvectors = np.array(eigenvectors)

        # b = P.T * (y-mean)
        mean_1d_normalized = np.ndarray.flatten(np.array(mean), order='F')

        b = np.matmul(eigenvectors, np.subtract(y, mean_1d_normalized))
        b = get_limited_beta(b, eigenvalues, 0.1)  # Limit beta

        # Ynew = Transform(Mean + P * b)
        ynew_1d = np.add(mean_1d_normalized, np.matmul(b, eigenvectors))
        ynew = np.reshape(ynew_1d, (int(len(ynew_1d) / 2), 2), order='F')

        Ynew, inverse_parameters = inverse_procrustes_transform(procrustes_transformed, ynew)
        Ynew = np.rint(Ynew).astype(int)

        if plotMinLambdaMaxLambda:
            plot_min_lambda_max_lambda(b, eigenvalues, eigenvectors, mean2, 0.25, 0.5)

        # Set latest suggested to the conformed ones
        # suggested_landmark_coordinates_2d = Ynew

        if showPlots and iteration % plotEveryIterations == 0:
            # x_offset = 1250  # 1100
            # w = 250 # 500 # 700
            # y_offset = 650
            # h = 350 # 700 # 800
            x_offset = 1200
            w = 300  # 500 # 700
            y_offset = 650
            h = 400  # 700 # 800
            Ynew_1d = np.ndarray.flatten(Ynew, order='F')
            plot_radiograph(mean_1d, Ynew_1d, test_image, x_offset, y_offset, h, w, suggested_landmark_coordinates_1d,
                            iteration)
            plot_incisor(mean_1d, Ynew_1d, x_offset, y_offset, suggested_landmark_coordinates_1d, iteration)

        if plotAllTrainingPoints:  # plot all training points
            plot_incisor_training_points(mean2, x2)

    # Output binary image polygon
    height, width = test_image.shape
    return Ynew, height, width


if __name__ == '__main__':

    # Parameters
    incisors = range(1, 9)  # Which incisors to detect
    training_radio_graphs = range(1, 15)  # Range of all training radiographs
    test_radio_graphs = range(1, 31) # Range of all testing radiographs
    k = 3  # Count of samples left and right of the init landmark. Total: 2k + 1
    m = 10  # Count of total new samples to test against the 2k + 1
    max_iterations = 71  # Total iterations per incisor

    # test_radio_graphs = [2] # Seen: radiograph: 2, k = 3, m = 10, iters = 71, incisors: 1
    # Not-seen: radiograph: 15, k = 6, m = 20, iters = 41, incisors: 1
    # test_radio_graphs = [15]
    # k = 6
    # m = 20
    # max_iterations = 41

    for test_radio_graph in test_radio_graphs:
        polygons = []
        height = 0
        width = 0
        for incisor in incisors:
            polygon, height, width = do_boundary_mahalanobis([incisor], training_radio_graphs, test_radio_graph, k, m, max_iterations)
            polygons.append(polygon)

        if storeBinaryDetection:  # Output binary image: polygon landmarks -> ones. Save 0/1 image
            img_binary_incisors = np.zeros((height, width, 3), np.uint8)
            img_segmentation_incisors = np.zeros((height, width, 3), np.uint8)
            # Superimpose or use directly the segmented
            if np.in1d(test_radio_graph, training_radio_graphs):
                for incisor in incisors:
                    new_img_segmentation_incisors = cv2.imread("data/images/segmentations/" + "{}{}-{}.png"
                        .format(test_radio_graph / 10, test_radio_graph % 10, incisor - 1))
                    img_segmentation_incisors = np.add(img_segmentation_incisors, new_img_segmentation_incisors)

            for polygon in polygons:
                cv2.fillPoly(img_binary_incisors, [polygon], (255, 255, 255))

            for polygon in polygons:
                cv2.fillPoly(img_segmentation_incisors, [polygon], (255, 255, 255))

            print "-- Storing binary detection for {}.tif".format(test_radio_graph)
            cv2.imwrite("binary_detected_incisors{}.png".format(test_radio_graph), img_binary_incisors)
            if np.in1d(test_radio_graph, training_radio_graphs):
                img_segmentation_incisors = np.add(img_segmentation_incisors, new_img_segmentation_incisors)
                print "-- Storing binary detection segmentations (ground truth) for {}.tif".format(test_radio_graph)
                cv2.imwrite("binary_detected_incisors_radiographs{}.png".format(test_radio_graph),
                            img_segmentation_incisors)

    print('Done.')

