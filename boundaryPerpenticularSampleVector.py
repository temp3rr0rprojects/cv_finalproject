import numpy as np
from math import sqrt


def find_closest_landmark(point, points):
    min_distance_point = [float('inf'), -1]

    for i in range(len(points)):
        current_point = points[i]
        if current_point[0] != point[0] and current_point[1] != point[1]:
            distance = round(sqrt((current_point[0] - point[0])**2 + (current_point[1] - point[1])**2), 3)
            if distance < min_distance_point[0]:
                min_distance_point = [distance, i]

    return points[min_distance_point[1]]


def find_line_slope_intercept(point1, point2):

    if point2[0] - point1[0] == 0:
        slope = 0  # Slope = x / 0, infinity -> slope = 0
    else:
        slope = float(point2[1] - point1[1]) / float(point2[0] - point1[0])

    intercept = point1[1] - slope * point1[0]
    return slope, intercept


def find_perpendicular_line_slope_intercept(point, slope, intercept):

    if slope == 0:  # Slope = x / 0, infinity -> slope = 0
        new_slope = 0
    else:
        new_slope = -1 / float(slope)

    new_intercept = point[1] - new_slope * point[0]
    return new_slope, new_intercept


def get_grid_sample_coordinates(point, slope, intercept, m, image):
    max_x = np.shape(image)[0] - 1
    max_y = np.shape(image)[1] - 1

    point = [int(point[0]), int(point[1])]
    x_init = point[0]
    sample_grid_coordinates = [point]

    if point[0] < len(image) and point[1] < len(image[0]):
        sample_values = [image[point[0]][point[1]]]

        x_before = x_init
        x_after = x_init
        sample_count = 1
        do_before = True
        while sample_count < m:
            if do_before:
                do_before = False
                x_before -= 1
                x = x_before
            else:
                do_before = True
                x_after += 1
                x = x_after

            if 0 <= x <= max_x:
                y = int(round(slope * x + intercept))
                if 0 <= y <= max_y:
                    new_point = [x, y]
                    if not sample_grid_coordinates.__contains__(new_point):

                        if not do_before:
                            sample_grid_coordinates.insert(0, new_point)
                            sample_values.insert(0, image[x][y])
                        else:
                            sample_grid_coordinates.append(new_point)
                            sample_values.append(image[x][y])
                        sample_count += 1
    else:  # Out of image range manipulation
        sample_values = np.zeros(m)

    return sample_grid_coordinates, sample_values


def get_samples_along_boundary(B, landmark_points, m, image):

    closest_landmark = find_closest_landmark(B, landmark_points)
    slope, intercept = find_line_slope_intercept(B, closest_landmark)
    new_slope, new_intercept = find_perpendicular_line_slope_intercept(B, slope, intercept)
    sample_grid_coordinates, sample_values = get_grid_sample_coordinates(B, new_slope, new_intercept, m, image)

    return sample_grid_coordinates, sample_values
