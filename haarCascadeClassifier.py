import copy, os, math
import cv2
import numpy as np


def get_new_incisor_starting_position(input_x, input_y, input_w, input_h, gray, search_area_percent, around_tooth_percent):

    x_found = input_x
    y_found = input_y
    w_found = input_w
    h_found = input_h

    x_offset = int(x_found * search_area_percent)
    y_offset = int(y_found * search_area_percent)
    w = int(w_found + (x_found * (1 - search_area_percent)) * 2)
    h = int(h_found + (y_found * (1 - search_area_percent)) * 2)
    crop_img = gray[y_offset:y_offset + h, x_offset:x_offset + w]
    gray = crop_img

    fx = fy = 1.0
    tooth_side_x = 100
    tooth_side_y = 260
    max_tooth_scale = 1.6
    num_neighbors = 1
    scale = 1.1

    model_name = "cascade"
    incisor1_cascade = cv2.CascadeClassifier(
        'HaarCascade/data/{}.xml'.format(model_name))  # Load the Haar frontal face file to the cascade classifier

    incisor_rectangles = incisor1_cascade.detectMultiScale(gray,
                                                  scaleFactor=scale,  # Parameter specifying how much the image size is reduced at each image scale.
                                                  minNeighbors=num_neighbors,  # Parameter specifying how many neighbors each candidate rectangle should have to retain it.
                                                  minSize=(int(tooth_side_x * fx), int(tooth_side_y * fy)), # Minimum possible object size. Objects smaller than that are ignored.
                                                  maxSize =(int(tooth_side_x * max_tooth_scale * fx), int(tooth_side_y * max_tooth_scale * fy)),  # Maximum possible object size. Objects smaller than that are ignored.
                                                  )  # Perform the teeth detection
    filtered_incisor_rectangles = []
    filtered_incisor_rectangle_distances = []

    for incisor_rectangle in incisor_rectangles:
        x, y, w, h = incisor_rectangle
        x += x_offset
        y += y_offset
        if abs(input_x - x) < w * around_tooth_percent and abs(input_y - y) < h * around_tooth_percent:  # Skip polygons outside of the wanted tooth size thresholds
            filtered_incisor_rectangles.append([x, y, w, h])
            filtered_incisor_rectangle_distances.append(math.sqrt((input_x - x)**2 + (input_y - y)**2))

    if len(filtered_incisor_rectangle_distances) == 0:
        return 0, 0  # If no polygon, zero transformation
    if len(filtered_incisor_rectangles) == 1:  # If 1 polygon, return polygon transformation
        return input_x - filtered_incisor_rectangles[0][0], input_y - filtered_incisor_rectangles[0][1]
    else:  # If > 1 polygons, return min distance polygon transformation
        return input_x - filtered_incisor_rectangles[np.argmin(filtered_incisor_rectangle_distances)][0], \
               input_y - filtered_incisor_rectangles[np.argmin(filtered_incisor_rectangle_distances)][1]