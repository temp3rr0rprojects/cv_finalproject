import numpy as np
from scipy.spatial.distance import mahalanobis
from sklearn.preprocessing import normalize


def get_mahalanobis(all_landmark_samples, current_sample):
    mean_landmark_samples = np.mean(all_landmark_samples, axis=0)
    cov_matrix = np.cov(np.transpose(all_landmark_samples))

    inv_cov_matrix = np.linalg.pinv(cov_matrix)
    distance = mahalanobis(current_sample, mean_landmark_samples, inv_cov_matrix)

    return distance


def get_mahalanobis_grayscale(all_landmark_samples, current_sample_m):
    all_landmark_samples = normalize(all_landmark_samples, axis=1)
    k = len(all_landmark_samples[0])
    m = len(current_sample_m)

    if m > k:
        min_distance_sample = [current_sample_m[0:k], float("inf"), 0]

        for i in range(0, m - k + 1):
            current_sample_k = current_sample_m[i:i + k]
            current_sample_k = normalize([current_sample_k], axis=1)[0]
            distance = get_mahalanobis(all_landmark_samples, current_sample_k)
            if distance < min_distance_sample[1]:
                min_distance_sample = [current_sample_k, distance, i]

        return min_distance_sample[0], min_distance_sample[1], min_distance_sample[2]
    else:
        pass