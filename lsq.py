import numpy as np
from sklearn.preprocessing import normalize

def get_sse(mean_landmark_positions, current_landmark_positions):
    sum_of_squared_errors = np.sum(np.subtract(mean_landmark_positions, current_landmark_positions) ** 2)
    return sum_of_squared_errors


def get_position_sse(mean_landmark_positions, potential_landmark_positions):
    mean_landmark_positions = normalize(mean_landmark_positions, axis=1)

    lsq = [potential_landmark_positions, float("inf")]
    for current_landmark_positions in potential_landmark_positions:
        current_landmark_positions = normalize(current_landmark_positions, axis=1)
        sse = get_sse(mean_landmark_positions, current_landmark_positions)
        if sse < lsq[1]:
            lsq = [current_landmark_positions, sse]

    return lsq[0], lsq[1]