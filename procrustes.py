import numpy as np
from scipy.spatial import procrustes


def procrustes_transform(X, Y, scaling=True, reflection='best'):
    """
    A port of MATLAB's `procrustes` function to Numpy.

    Procrustes analysis determines a linear transformation (translation,
    reflection, orthogonal rotation and scaling) of the points in Y to best
    conform them to the points in matrix X, using the sum of squared errors
    as the goodness of fit criterion.

        d, Z, [tform] = procrustes(X, Y)

    Inputs:
    ------------
    X, Y
        matrices of target and input coordinates. they must have equal
        numbers of  points (rows), but Y may have fewer dimensions
        (columns) than X.

    scaling
        if False, the scaling component of the transformation is forced
        to 1

    reflection
        if 'best' (default), the transformation solution may or may not
        include a reflection component, depending on which fits the data
        best. setting reflection to True or False forces a solution with
        reflection or no reflection respectively.

    Outputs
    ------------
    d
        the residual sum of squared errors, normalized according to a
        measure of the scale of X, ((X - X.mean(0))**2).sum()

    Z
        the matrix of transformed Y-values

    tform
        a dict specifying the rotation, translation and scaling that
        maps X --> Y

    """
    n,m = X.shape
    ny,my = Y.shape

    muX = X.mean(0)
    muY = Y.mean(0)

    X0 = X - muX
    Y0 = Y - muY

    ssX = (X0**2.).sum()
    ssY = (Y0**2.).sum()

    # centred Frobenius norm
    normX = np.sqrt(ssX)
    normY = np.sqrt(ssY)

    # scale to equal (unit) norm
    X0 /= normX
    Y0 /= normY

    if my < m:
        Y0 = np.concatenate((Y0, np.zeros(n, m-my)),0)

    # optimum rotation matrix of Y
    A = np.dot(X0.T, Y0)
    U,s,Vt = np.linalg.svd(A,full_matrices=False)
    V = Vt.T
    T = np.dot(V, U.T)

    if reflection is not 'best':

        have_reflection = np.linalg.det(T) < 0  # does the current solution use a reflection?

        if reflection != have_reflection:  # if that's not what was specified, force another reflection
            V[:,-1] *= -1
            s[-1] *= -1
            T = np.dot(V, U.T)

    traceTA = s.sum()

    if scaling:
        b = traceTA * normX / normY  # optimum scaling of Y
        d = 1 - traceTA**2  # standardised distance between X and b*Y*T + c
        Z = normX*traceTA*np.dot(Y0, T) + muX  # transformed coords

    else:
        b = 1
        d = 1 + ssY/ssX - 2 * traceTA * normY / normX
        Z = normY*np.dot(Y0, T) + muX

    # transformation matrix
    if my < m:
        T = T[:my,:]
    c = muX - b*np.dot(muY, T)

    tform = {'rotation':T, 'scale':b, 'translation':c}  # transformation values

    return d, Z, tform


def inverse_procrustes_transform(parameters, input):
    inverse_parameters = {'translation': np.multiply(-1, parameters['translation']),
                          'scale': 1 / float(parameters['scale']), 'rotation': np.linalg.inv(parameters['rotation'])}

    scaled_rotated = np.matmul(np.add(inverse_parameters['translation'], input),
                               np.multiply(inverse_parameters['scale'], inverse_parameters['rotation']))

    return scaled_rotated, parameters

def list_1d_to_2d(x):
    returning_x = []
    for shape_1d in x:
        transformed_x_shape = []
        for index in range(len(shape_1d) / 2):
            transformed_x_shape.append([shape_1d[index], shape_1d[index + len(shape_1d) / 2]])
        returning_x.append(transformed_x_shape)

    return returning_x


def list_2d_to_1d(x):
    returning_x = []
    for shape_2d in x:
        shape_1d = []
        for index in range(len(shape_2d)):
            shape_1d.append(shape_2d[index][0])
        for index in range(len(shape_2d)):
            shape_1d.append(shape_2d[index][1])
        returning_x.append(shape_1d)
    return returning_x


def get_generalized_procrustes_analysis_transform(X):

    transformed_x = list_1d_to_2d(X)

    reference_shape = transformed_x[0]
    mean_disparity_threshold = 0.05
    mean_disparity = mean_disparity_threshold

    while mean_disparity >= mean_disparity_threshold:
        sum_disparities = 0
        for index in range(len(transformed_x)):
            transformed_reference, transformed_x[index], disparity = procrustes(reference_shape, transformed_x[index])
            sum_disparities += disparity

        reference_shape = np.mean(transformed_x, axis=0)    # New reference shape is the mean

        mean_disparity = sum_disparities / len(transformed_x)

    returning_x = list_2d_to_1d(transformed_x)

    return returning_x