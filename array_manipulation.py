import numpy as np
import copy
from procrustes import procrustes_transform, inverse_procrustes_transform

A = np.array([1600, 1630, 1520, 693, 563, 531])

print "A: {}".format(A)

x_transform = -10000
y_transform = 5000
A[:len(A) / 2] += x_transform
A[len(A) / 2:] += y_transform
print "A transformed: {}".format(A)

B = np.reshape(A, (int(len(A)/2), 2), order='F')
print "B: {}".format(B)

# A_back = np.reshape(B, len(B), order='F')
A_back = np.ndarray.flatten(B, order='F')
print "Equality: {}".format(np.array_equal(A, A_back))


B = np.reshape(A, (int(len(A)/2), 2), order='F')
print "B: {}".format(B)


# x_all = mean_1d[:len(mean_1d) / 2]  # Even: x1, ..., xn
C = A[:len(A)/2]

print "C: {}".format(C)

D = A[len(A) / 2:]
print "D: {}".format(D)

A2 = np.array([[1600, 1630, 1520, 693, 563, 531], [1600, 1630, 1520, 693, 563, 531]])
print "A2: {}".format(A2)
A2_copy = copy.copy(A2)
x_transform = -10000
y_transform = 5000
for row in A2:
    row[:len(row) / 2] += x_transform
    row[len(row) / 2:] += y_transform
print "A transformed: {}".format(A2)
print "A2_copy: {}".format(A2_copy)
print "Diffs: {} {}".format(A2[0][0] - A2_copy[0][0], A2[0][len(A2[0]) - 1] - A2_copy[0][len(A2[0]) - 1])

# for A22 in A2:
#     np.reshape(A, (int(len(A22) / 2), 2), order='F')

B2 = [np.reshape(row, (int(len(row)/2), 2), order='F') for row in A2]

C = np.array([[160, 61], [170, 71]])
D = np.array([[17, 7], [1600, 610]])
print "\nC: {}".format(C)
print "D: {}".format(D)
residual_sse, y, procrustes_transformed = procrustes_transform(C, D)
print "y: {}".format(y)
C_all = [[160, 61], [163, 68],  [165, 65],  [169, 70]]
C_new, params = inverse_procrustes_transform(procrustes_transformed, C_all)
print "C_new: {}".format(C_new)

C = np.array([[1560, 610], [1650, 710]])
D = np.array([[1480, 710], [1800, 910]])
print "\nC: {}".format(C)
print "D: {}".format(D)
residual_sse, y, procrustes_transformed = procrustes_transform(C, D)
print "y: {}".format(y)
C_all = [[1560, 610], [1580, 690], [1630, 680], [1643, 708], [1650, 710]]
C_new, params = inverse_procrustes_transform(procrustes_transformed, C_all)
print "C_new: {}".format(C_new)
