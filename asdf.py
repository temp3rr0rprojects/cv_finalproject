from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import cv2

im1 = cv2.imread("02.png")

x_offset = 200  # 1100
w = 400 # 500 # 700
y_offset = 200
h = 300 # 700 # 800
# x_offset = 1200
# w = 300  # 500 # 700
# y_offset = 650
# h = 400  # 700 # 800

im1 = im1[y_offset:y_offset + h, x_offset:x_offset + w]
cv2.imwrite("02_cropped.png", im1)

im1 = cv2.imread("denoised_02.png")
# x_offset = 200  # 1100
# w = 400 # 500 # 700
# y_offset = 200
# h = 300 # 700 # 800
im1 = im1[y_offset:y_offset + h, x_offset:x_offset + w]
cv2.imwrite("denoised_02_cropped.png", im1)