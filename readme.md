#Final Project: Incisor Segmentation

In forensic investigations, dental records are often relied upon to accurately identify decease dpersons.
Manual comparison of ante-mortem and post-mortem records, however,is a tedious and time-consuming job.
Automating this process would save the investigators a lot of time.
The first step towards a comparison is segmentation of the teeth.
The goal of this project is to develop a model-based segmentation approach, capable of segmenting the upper and lower incisors in panoramic radiographs.

* Python version used: 2.7
* Additional python libraries: cv2, scipy.spatial, sklearn.preprocessing, numpy, os, copy, math
* IDE: PyCharm 2018.1


##Project goals

1. Build an Active Shape Model as described in the original paper by  Cootes et al. (see [1] and [2])

2. Pre-process the dental radiographs

3. Fit the model to an image

4. Evaluate your results

5. Report your results

## Source Code

This project was realised by one person:
Konstantinos Theodorakos

**Python version:** 2.7

**IDE:** PyCharm version 2018.1

**Bibucket git repository:** https://bitbucket.org/temp3rr0rprojects/cv_finalproject/src

#####Libraries:
* os, copy, math, cv2, numpy
* sklearn.preprocessing
* scipy.spatial
* matplotlib.pyplot
* matplotlib.cm

#####Data Placements:

* **Radiographs** (directory *data\images\radiographs*): 01.tif to 14.tif

* **Extra Radiographs** (directory *data\images\radiographs\extra*): 15.tif to 30.tif

* **Segmentations** (directory *data\images\segmentations*): 01-0.png to 14-7.png

* **Landmarks** (directory *data\text\landmarks*): landmarks1-1.txt to landmarks28-8.txt

##### How to run ASM incisor segmentations:
* **Execute:** *skeleton.py*

### Extra: Haar Cascade classifier model

#####Data placements:
* **Denoised Image Locations** (directory *HaarCascade\img*): denoised_01.tif to denoised_14.tif (essentially 01.tif to 14.tif, de-noised and renamed)

#####Files and libraries placement

Download (x64 windows binaries): https://sourceforge.net/projects/opencvlibrary/files/opencv-win/3.4.1/

**Executables** and **libraries** placement (directory *\HaarCascade*):
* opencv_createsamples.exe
* opencv_ffmpeg341_64.dll
* opencv_interactive-calibration.exe
* opencv_traincascade.exe
* opencv_version.exe
* opencv_visualisation.exe
* opencv_world341.dll
* opencv_world341d.dll

#####Commands (window cli):
* **Create training vectors:** *.\opencv_createsamples.exe -info .\info.dat -num 14 -w 40 -h 105 -vec incisors8.vec*

* **View vectors:** *.\opencv_createsamples.exe -info .\info.dat -w 40 -h 105 -vec incisors8.vec*

* **Train (LBP faster than haar cascade):** *.\opencv_traincascade.exe -data data -vec .\incisors8.vec -bg .\bg.txt -numPos 10 -numNeg 10 -numStages 30 -w 40 -h 105 -featureType LBP*



####References

[1] Active Shape Models – their training and application

[2] An introduction to Active Shape Models

[3] A statistical method for robust 3D surface reconstruction from sparse data

[4] Model reconstruction